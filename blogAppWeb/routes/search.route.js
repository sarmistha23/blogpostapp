const { searchController } = require('../controller/index');
const router = require('express').Router();

router.get('/searchUser', searchController.searchBarUser);

router.get('/searchBlog', searchController.searchPosts);


module.exports = router;
