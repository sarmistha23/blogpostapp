const {googleLogin}  = require('../controller/google.controller');
const express = require('express');
const router = express.Router();

const passport  = require('passport');
const GoogleStrategy = require('passport-google-oauth20').Strategy;

const clientId = "233240500848-00o48f7qirlakqjclso5e6gn5gidqb4d.apps.googleusercontent.com";
const clientKey = "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";



router.use(passport.initialize());


passport.use(
    new GoogleStrategy({
        clientID: clientId,
        clientSecret: clientKey,
        callbackURL: '/auth/google/callback'
    },
    (accessToken, refreshToken, profile, done) => {
        console.log(profile, 'profile');
        return done(null, profile)
    })
);


router.get('/auth/google', passport.authenticate('google', {
    scope: ['profile', 'email']
    })
);


passport.serializeUser((user, done) => {
	done(null, user);
});

passport.deserializeUser((user, done) => {
	done(null, user);
});

router.get(
	"/auth/google/callback",
	passport.authenticate("google", { failureRedirect: "/failed" }),
	googleLogin
);

router.get("/failed", (req, res) => {
    res.send("Failed")
})


module.exports = router;