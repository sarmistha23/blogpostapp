const pageController = require('../controller/page');
const Auth = require('../middleware/Auth');
const router = require('express').Router();

// console.log(pageController.enterPassword);

router.get('/userPage', pageController.pagination);


router.post('/otpVerify', Auth, pageController.otpVerification);

router.put('/setPassword', Auth, pageController.enterPassword);


module.exports = router;