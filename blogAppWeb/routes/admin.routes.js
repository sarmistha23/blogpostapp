const { adminController } = require('../controller/index');
const { adminControlUserBlog } = require('../controller/index');
const Auth = require('../middleware/Auth');
const router = require('express').Router();


router.post('/admin/signup', adminController.adminSignup);
router.post('/admin/login', adminController.adminLogin);
router.delete('/admin/blogDelete', Auth, adminControlUserBlog.adminDeleteUsersBlog);
router.delete('/admin/userDelete', Auth, adminControlUserBlog.adminDeleteUserAcc);
router.get('/admin/allUsers', Auth, adminController.AllUsersProfile);

module.exports = router;