const router = require('express').Router();

const userRoute = require('./user.route');
const blogRoute = require('./blog.route');
const commentRoute = require('./comment.route');
const searchRoute = require('./search.route');
const adminRoute = require('./admin.routes');
// const googleAuth = require('./googleSchema');
// user route
router.use('/', userRoute);

// blog route
router.use('/', blogRoute);

// comments route
router.use('/', commentRoute);

router.use('/', searchRoute);

// router.use('/', googleAuth);

// admin routes
router.use('/', adminRoute);


module.exports = router;