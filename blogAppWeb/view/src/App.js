import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import UploadProfile from './components/profile/Profile';
import ProtectedRoute from './components/protected';
import Comment from './components/comment/comments';
import AllUser from './components/allpost/AllUser';
import AllPostBlogs from './components/allpost/AllBlogPosts.js';
// import Navbar from './components/Navbar';
import EditBlog from './components/Blog/Editblog';
import Nav from './components/Navbar/Navbar';
import PaginationPage from './components/pagination/page'
import ErrorPage from './components/Homepage/Error';
import AllPosts from './components/allpost/ShowPosts'

// with template Route

import Login from './components/Login/Login';
import Signup from './components/Signup/Signup';
import UserProfile from './components/profile/UserProfile';
import NewProfile from './components/profile/newprofile';
import Footer from './components/layout/Footer';


// admin pages
import AdminSignup from './components/Admin/Signupadmin';
import AdminLogin from './components/Admin/loginadmin';
 

// With Template UI 
import LogIn from './components/Login/TemplateLogin';
import SignUp from './components/Signup/Sign';
import Blog from './components/Blog/BlogCreate';
import EditProfileUi from './components/profile/Editprofile';
import GetOneUser from './components/Blog/Bloguser';
import GetUserProfile from './components/allpost/User';
import Homepage from './components/Homepage/Homepage';
import VerifyEmail from './components/forgetpassword/Verification';
import OtpVerification from './components/forgetpassword/OtpScreen';
import EnterPassword from './components/forgetpassword/Password';
import SearchBar from './components/searchbar/Searchbar';
import HeaderSideBar from './components/layout/HeaderSide';
import AlertComponent from './components/Homepage/Alert';
import CreateUserProfile from './components/profile/CreateuserProfile';
import AdminCheckAllUsersData from './components/Admin/Allusers';
import AdminLogoUtPage from './components/Admin/Adminlogout';
import BlogControl from './components/Admin/Blog';

//WITH THEME 
import ProfileEdit from './components/profile/ProfileEdit';


const Routing = () => {

  return (


    <Switch>

      {/* <Route path='/userPro'><UserProfile/></Route> 
      <Route path='/profile'><Profile/></Route> */}


      {/* with THEME */}

      <Route path='/log'><Login /></Route>

      <Route path='/sign'><Signup /></Route>

      <ProtectedRoute path='/newprofile'><NewProfile /></ProtectedRoute>

      <Route path='/eprofile'><ProfileEdit /></Route>

      <Route path='/header'><HeaderSideBar /></Route>

      <Route path='/show'><AllPosts /></Route>

      {/* <Route path='/create'><Blog /></Route> */}
      
      <ProtectedRoute path='/create' component={Blog}></ProtectedRoute>

      <Route path='/alert'><AlertComponent/></Route>

      <Route path='/profile'><CreateUserProfile/></Route>

      <Route path='/allData'><AdminCheckAllUsersData/></Route>

      <Route path='/adminLogout'><AdminLogoUtPage/></Route>

      <Route path='/blogControl'><BlogControl/></Route>
      {/* admin routes */}

      <Route path='/admin/sign'><AdminSignup/></Route>
      <Route path='/admin/log'><AdminLogin/></Route>


      {/* With Template UI  */}

      <Route path='/error'><ErrorPage /></Route>
      <Route path='/home'><Homepage /></Route>

      <Route path='/allusers'><AllUser/></Route>
      <Route exact path='/search'><SearchBar /></Route>

      {/* <Route path='/sign'><SignUp /></Route>

      <Route path='/login'><LogIn /></Route> */}

      <Route exact path='/'><HeaderSideBar /><AllPostBlogs /></Route>
      <Route path='/page'><PaginationPage /></Route>

      <Route path='/verify'><VerifyEmail /></Route>

      <Route path='/screen'><OtpVerification /></Route>

      <Route path='/password'><EnterPassword /></Route>

      {/* <ProtectedRoute exact path='/blog' component={BlogCreate} /> */}

      {/* <Route exact path='/' component={AllPostBlogs} /> */}

      <ProtectedRoute path='/profileimg' component={UploadProfile} />

      <ProtectedRoute exact path='/updateProfile' component={EditProfileUi} />

      <ProtectedRoute path='/userBlog' component={GetOneUser} />

      <ProtectedRoute exact path='/editBlog' component={EditBlog} />

      <ProtectedRoute exact path='/userprofile' component={GetUserProfile} />

    </Switch>
  );
};


function App() {

  return (

    <Router>
      {/* {} */}
      {/* <HeaderSideBar /> */}

      <Routing />

      {/* <Footer/> */}

    </Router>

  );
};



export default App;