import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import "bootstrap/dist/css/bootstrap.min.css";

const Login = () => {

    const history = useHistory();
    // const { state, dispatch } = useContext(userContext);
    const [errorMessage, setErrorMessage] = useState('');

    const [user, setUser] = useState({
        email: "",
        password: ""
    });

    const handleChange = e => {
        // console.log('hello login');
        const { name, value } = e.target
        setUser({
            ...user,
            [name]: value
        })
    };

    // console.log(user, 'user');

    const logIn = async (event) => {
        event.preventDefault();
        // console.log('hello login user');

        try {

            const result = await axios.post('http://127.0.0.1:8080/login', user);
            // console.log('hello login user');
            // console.log(result, 'result');
            if (result.data.message == "logged in Successful") {
                localStorage.setItem('Token', result.data.Token)
                localStorage.setItem('isAuthenticated', true)
                history.push('/')
            }
            if (result.data.message == 'username or email something is wrong') {
                history.push('/sign');
            } 
            if (result.data.message == false) {
                alert('wrong password')
            }

        } catch (error) {
            console.log(error.response.data.message);
            setErrorMessage(error.response.data.message)
        }
    }

    // const resetInputField = () => {
    //     setUser({
    //         email: "",
    //         password: ""
    //     });
    // };


    return (
        <div className="container">
            <section className="section register min-vh-100 d-flex flex-column align-items-center justify-content-center py-4">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-lg-4 col-md-6 d-flex flex-column align-items-center justify-content-center">
                            <div className="d-flex justify-content-center py-4">
                                <a href="index.html" className="logo d-flex align-items-center w-auto">
                                    <img src="assets/img/logo.png" alt="" />
                                    <span className="d-none d-lg-block">BlogApp</span>
                                </a>
                            </div>
                            <div className="card mb-3">
                                <div className="card-body">
                                    <div className="pt-4 pb-2">
                                        <h5 className="card-title text-center pb-0 fs-4">Login to Your Account</h5>
                                        <p className="text-center small">Enter your username & password to login</p>
                                    </div>
                                    <form className="row g-3 needs-validation" >
                                        <div className="col-12">
                                            <label for="yourUsername" className="form-label">Email</label>
                                            <div className="input-group has-validation">
                                                <span className="input-group-text" id="inputGroupPrepend">@</span>
                                                <input type="email"
                                                    name="email"
                                                    className="form-control"
                                                    // id="yourUserEmail"
                                                    placeholder="example@gmail.com"
                                                    value={user.email}
                                                    onChange={handleChange}
                                                    required
                                                />
                                                <div className="invalid-feedback">Please enter your Email</div>
                                            </div>
                                        </div>

                                        <div className="col-12">
                                            <label for="yourPassword" className="form-label">Password</label>
                                            <input type="password"
                                                name="password"
                                                className="form-control"
                                                // id="yourPassword"
                                                placeholder="password"
                                                value={user.password}
                                                onChange={handleChange}
                                                required
                                            />
                                            <div className="invalid-feedback">Please enter your password!</div>
                                        </div>

                                        <div className="col-12">
                                            <div className="form-check">
                                                <input className="form-check-input" type="checkbox" name="remember" value="true" id="rememberMe" />
                                                <label className="form-check-label" for="rememberMe">Remember me</label>
                                            </div>
                                        </div>
                                        <div className="col-12">
                                            <button className="btn btn-primary w-100" type="submit" onClick={logIn}>Login</button>
                                        </div>
                                        <div className="col-12">
                                            <p className="small mb-0">Don't have account? <a href="/sign">Create an account</a></p>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    );
};


export default Login;