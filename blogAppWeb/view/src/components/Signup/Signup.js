import { useHistory } from 'react-router-dom';
import React, { useState } from 'react';
import axios from 'axios';


const Signup = () => {

    const history = useHistory();
    const [errorMessage, setErrorMessage] = useState();
    const [success, setSuccess] = useState();

    const [user, setUser] = useState({
        name: "",
        email: "",
        password: "",
        gender: "",
        location: "",
    })

    const handleChange = e => {
        // console.log(e.target)
        const { name, value } = e.target
        setUser({
            ...user,
            [name]: value
        })
    }

    console.log(user, 'user data');

    const register = async (event) => {
        event.preventDefault();
        console.log('hello sign up');
        try {
            const res = await axios.post('http://127.0.0.1:8080/signup', user);
            setSuccess(res.data.message);
            history.push('/log');
        } catch (err) {
            // console.log("inside catch");
            setErrorMessage(err.response.data.message);
            // console.log(err.response.data.message);
        }
    }

    return (
        <div className="container">
            <section className="section register min-vh-100 d-flex flex-column align-items-center justify-content-center py-4">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-lg-4 col-md-6 d-flex flex-column align-items-center justify-content-center">
                            <div className="d-flex justify-content-center py-4">
                                <a href="index.html" className="logo d-flex align-items-center w-auto">
                                    <img src="assets/img/logo.png" alt="" />
                                    <span className="d-none d-lg-block">Blog App</span>
                                </a>
                            </div>
                            <div className="card mb-3">
                                <div className="card-body">
                                    <div className="pt-4 pb-2">
                                        <h5 className="card-title text-center pb-0 fs-4">Create an Account</h5>
                                        <p className="text-center small">Enter your personal details to create account</p>
                                    </div>
                                    <form className="row g-3 needs-validation" validate>
                                        <div className="col-12">
                                            <label for="yourName" className="form-label">Your Name</label>
                                            <input type="text" 
                                            name="name" 
                                            className="form-control" 
                                            id="yourName" 
                                            required
                                            value={user.name}
                                            onChange={handleChange}
                                             />
                                            <div className="invalid-feedback">Please choose a username.</div>
                                        </div>
                                        <div className="col-12">
                                            <label for="yourUsername" className="form-label">User Email</label>
                                            <div className="input-group has-validation">
                                                <span className="input-group-text" id="inputGroupPrepend">@</span>
                                                <input type="email" 
                                                name="email" 
                                                className="form-control" 
                                                id="UserEmail"
                                                required
                                                value={user.email}
                                                onChange={handleChange}
                                                 />
                                                <div className="invalid-feedback">Please, enter your name!</div>
                                            </div>
                                        </div>
                                        <div className="col-12">
                                            <label for="yourEmail" className="form-label">Your Password</label>
                                            <input type="password" 
                                            name="password" 
                                            className="form-control" 
                                            id="yourPassword"
                                            required
                                            value={user.password}
                                            onChange={handleChange}
                                             />
                                            <div className="invalid-feedback">Please, enter correct Password!</div>
                                        </div>
                                        <div className="col-12">
                                            <label for="Gender" className="form-label">Gender</label>
                                            <input type="text" 
                                            name="gender" 
                                            className="form-control" 
                                            id="gender"
                                            required
                                            value={user.gender}
                                            onChange={handleChange}
                                             />
                                            <div className="invalid-feedback">choose correct gender</div>
                                        </div>
                                        <div className="col-12">
                                            <label for="location" className="form-label">Location</label>
                                            <input type="text" 
                                            name="location" 
                                            className="form-control" 
                                            id="yourLocation"
                                            required
                                            value={user.location}
                                            onChange={handleChange}
                                             />
                                            <div className="invalid-feedback">Please, choose your location!</div>
                                        </div>
                                        <div className="col-12">
                                            <div className="form-check">
                                                <input className="form-check-input" name="terms" type="checkbox" value="" id="acceptTerms"  />
                                                <label className="form-check-label" for="acceptTerms">I agree and accept the <a href="#">terms and conditions</a></label>
                                                <div className="invalid-feedback">You must agree before submitting.</div>
                                            </div>
                                        </div>
                                        <div className="col-12">
                                            <button className="btn btn-primary w-100" type="submit" onClick={register}>Create Account</button>
                                            {errorMessage && <div className="error"> {errorMessage} </div>}
                                        </div>
                                        <div className="col-12">
                                            <p className="small mb-0">Already have an account? <a href="/log">Log in</a></p>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    )
};

export default Signup;