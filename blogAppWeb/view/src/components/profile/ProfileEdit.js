import React, { useState } from 'react';
import './profile.css';
import axios from 'axios';


const styles = {
    preview: {
        marginTop: 1,
        display: "flex",
        flexDirection: "column",
    },
    image: {
        position: 'relative',
        width: '100px',
        height: '100px',
        'border-radius': '200px',
    }
};


const ProfileEdit = (props) => {

    // console.log(props.data);

    // console.log('hello profile');

    const [bio, setBio] = useState(props.data.bio);
    const [image, setImage] = useState(props.data.image);

    const formData = new FormData();

    const Token = localStorage.getItem('Token');

    const EditUserProfile = e => {

        // console.log('hello edit');

        formData.append('bio', bio);
        formData.append('file', image);


        const config = {
            headers: {
                authorization: Token
            }
        };

        axios.put('http://127.0.0.1:8080/editprofile', formData, config)

            .then((result) => {
                console.log('hello then');
                console.log(result.data, 'mmmmmmm');
                alert("success");
            }).catch((err) => {
                console.log(err, 'jjjjjjj');
            })
    }


    const imageChange = (e) => {
        if (e.target.files && e.target.files.length > 0) {
            setImage(e.target.files[0]);
        }
    };

    const removeSelectedImage = () => {
        setImage();
    };

    return (
        <div>
            <div class="row mb-3">
                <label for="profileImage" class="col-md-4 col-lg-3 col-form-label">Profile Image</label>
                <div class="col-md-8 col-lg-9">
                    {image && (
                        <div style={styles.preview}>
                            <img
                                src={URL.createObjectURL(image)}
                                style={styles.image}
                                alt="Thumb"
                            />
                        </div>
                    )}
                    {!image && (
                        <input
                            accept="image/*"
                            type="file"
                            onChange={imageChange}
                        />
                    )}
                    <div class="pt-2">

                        <a href="#" class="btn btn-danger btn-sm" title="Remove my profile image" onClick={removeSelectedImage}><i class="bi bi-trash"></i></a>
                    </div>
                </div>
            </div>
            <div class="row mb-3">
                <label for="about" class="col-md-4 col-lg-3 col-form-label">About</label>
                <div class="col-md-8 col-lg-9">
                    <textarea name="about" class="form-control" id="about" style={{ "height": "100px" }} value={bio} onChange={event => {
                            // const value = event.target.value
                            setBio(event.target.value)
                        }}>
                        {/* <input type="textarea"  value={bio} id="bio" onChange={event => {
                            // const value = event.target.value
                            setBio(event.target.value)
                        }} /> */}
                        
                    </textarea>
                </div>
            </div>
            <div class="text-center">
                <button class="btn btn-primary" onClick={EditUserProfile}>Save Changes</button>
            </div>
        </div>

        // <main id="main" class="main">
        //     <section class="section profile">
        //         <div class="row">
        //             <div class="col-xl-8">
        //                 <div class="card">
        //                     <div class="card-body pt-3">
        //                         <ul class="nav nav-tabs nav-tabs-bordered">
        //                             <li class="nav-item">
        //                                 <button class="nav-link" data-bs-toggle="tab" data-bs-target="#profile-edit">Edit Profile</button>
        //                             </li>
        //                         </ul>
        //                         <div class="tab-content pt-2">
        //                             <div class="tab-pane fade profile-edit pt-3" id="profile-edit">
        //                                 <form>
        //                                     <div class="row mb-3">
        //                                         <label for="profileImage" class="col-md-4 col-lg-3 col-form-label">Profile Image</label>
        //                                         <div class="col-md-8 col-lg-9">
        //                                             {/* <img src="" alt="Profile" /> */}
        //                                             <div class="pt-2">
        //                                                 <a href="#" class="btn btn-primary btn-sm" title="Upload new profile image" onClick={imageChange}><i class="bi bi-upload"></i>
        //                                                     {image && (
        //                                                         <div style={styles.preview}>
        //                                                             <img
        //                                                                 src={URL.createObjectURL(image)}
        //                                                                 style={styles.image}
        //                                                                 alt="Thumb"
        //                                                             />
        //                                                         </div>
        //                                                     )}
        //                                                     {!image && (
        //                                                         <input
        //                                                             accept="image/*"
        //                                                             type="file"
        //                                                             onChange={imageChange}
        //                                                         />
        //                                                     )}
        //                                                 </a>
        //                                                 <a href="#" class="btn btn-danger btn-sm" title="Remove my profile image" onClick={removeSelectedImage}><i class="bi bi-trash"></i></a>
        //                                             </div>
        //                                         </div>
        //                                     </div>
        //                                     <div class="row mb-3">
        //                                         <label for="about" class="col-md-4 col-lg-3 col-form-label">About</label>
        //                                         <div class="col-md-8 col-lg-9">
        //                                             <textarea name="about" class="form-control" id="about" style={{ "height": "100px" }}>Sunt est soluta temporibus accusantium neque nam maiores cumque temporibus. Tempora libero non est unde veniam est qui dolor. Ut sunt iure rerum quae quisquam autem eveniet perspiciatis odit. Fuga sequi sed ea saepe at unde.</textarea>
        //                                         </div>
        //                                     </div>
        //                                     <div class="text-center">
        //                                         <button type="submit" class="btn btn-primary">Save Changes</button>
        //                                     </div>
        //                                 </form>
        //                             </div>
        //                         </div>
        //                     </div>
        //                 </div>
        //             </div>
        //         </div>
        //     </section>
        // </main>
    );
};


export default ProfileEdit;