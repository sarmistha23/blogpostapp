import React, { useState } from 'react';
import './profile.css';
import { useCookies } from 'react-cookie';
import axios from 'axios';
import { Avatar } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import { Button } from 'antd';


const styles = {
    preview: {
        marginTop: 50,
        display: "flex",
        flexDirection: "column",
    },
    image: {
        position: 'relative',
        'background-color': '#09f',
        margin: '20px auto',
        width: '200px',
        height: '200px',
        'border-radius': '200px',
    }
};



const EditProfileUi = () => {
    const [bio, setBio] = useState('');
    const [cookies, setCookies] = useCookies(["Token"]);
    const [image, setImage] = useState('');

    const formData = new FormData();

    const Token = localStorage.getItem('Token');
    const EditUserProfile = e => {
        formData.append('bio', bio);
        formData.append('file', image);


        const config = {
            headers: {
                authorization: Token
            }
        };

        axios.put('http://127.0.0.1:8080/editprofile', formData, config)
            .then((result) => {
                console.log(result.data, 'mmmmmmm');
                alert("success")
            }).catch((err) => {
                console.log(err, 'jjjjjjj');
            })
    }


    const imageChange = (e) => {
        if (e.target.files && e.target.files.length > 0) {
            setImage(e.target.files[0]);
        }
    };

    const removeSelectedImage = () => {
        setImage();
    };


    return (

        <div className='centered'>
            <div className='card text-center'>
            <div className="card" style={{ width: "18rem", backgroundColor: 'lightgrey' }}>
                <h4> Edit Profile</h4>
                <div id="circle">
                    <Avatar size={150} icon={<UserOutlined />} />
                    <div id="text" >
                        <style dangerouslySetInnerHTML={{ __html: "\nimg {\n  filter: contrast(80%);\n}\n" }} />
                        {image && (
                            <div style={styles.preview}>
                                <img
                                    src={URL.createObjectURL(image)}
                                    style={styles.image}
                                    alt="Thumb"
                                />
                            </div>
                        )}
                        {!image && (
                            <input
                                accept="image/*"
                                type="file"
                                onChange={imageChange}
                            />
                        )}
                    </div>
                </div>
                <br />
                <br />
                <div className="card-body">
                    <div>
                        <input type="text" id="bio" placeholder="write your bio" onChange={event => {
                            const { value } = event.target
                            setBio(value)
                        }} />
                    </div>
                </div>

                <Button onClick={removeSelectedImage} style={{ backgroundColor: 'red' }}>
                    Remove This Image
                </Button>
                <Button onClick={EditUserProfile} style={{ backgroundColor: 'lightgreen' }}>
                    EditProfile
                </Button>

            </div>
            </div>

        </div>

    )
}



export default EditProfileUi;