import React, { useState, useEffect } from "react";
import ProfileEdit from "./ProfileEdit";
import axios from "axios";


const NewProfile = (props) => {

    const [image, setImage] = useState('');
    const [name, setName] = useState('');
    const [bio, setBio] = useState('');
    const [email, setEmail] = useState('')
    const [location, setLocation] = useState('');
    const [content, setContent] = useState({ data: [] })
    const [state, setState] = useState('')
    const Token = localStorage.getItem('Token');

    const config = {
        headers: {
            authorization: Token
        }
    };

    const userData = async () => {
        await axios.get('http://127.0.0.1:8080/userprofile', config)
            .then((resp) => {
                console.log(resp.data.userProfile.userId.location, 'userprofile data')
                setContent({ data: resp.data.userProfile })
                setName(resp.data.userProfile.userId.name);
                setBio(resp.data.userProfile.bio);
                setLocation(resp.data.userProfile.userId.location);
                setEmail(resp.data.userProfile.userId.email);
                setImage(`http://127.0.0.1:8080/${resp.data.userProfile.image}`)
            })
            .catch((err) => {
                console.log(err);
            })
    }

    useEffect(() => {
        userData()
        return (()=>{
            return setState("")
        })
    }, []);

    // console.log(image, 'profile image');

    return (

        <main id="main" className="main">
            <section className="section profile">
                <div className="row">
                    <div className="col-xl-4">
                        <div className="card">
                            <div className="card-body profile-card pt-4 d-flex flex-column align-items-center">
                                <img src={image} alt="Profile" className="rounded-circle" />
                                <h2>{name}</h2>
                                <h3>Web Designer</h3>
                            </div>
                        </div>
                    </div>
                    <div className="col-xl-8">
                        <div className="card">
                            <div className="card-body pt-3">
                                <ul className="nav nav-tabs nav-tabs-bordered">
                                    <li className="nav-item">
                                        <button className="nav-link active" data-bs-toggle="tab" data-bs-target="#profile-overview">Overview</button>
                                    </li>
                                    <li className="nav-item">
                                        <button className="nav-link" data-bs-toggle="tab" data-bs-target="#profile-edit">Edit Profile</button>
                                    </li>
                                </ul>
                                <div className="tab-content pt-2">
                                    <div className="tab-pane fade show active profile-overview" id="profile-overview">
                                        <h5 className="card-title">About</h5>
                                        <p className="small fst-italic">{bio}</p>
                                        <h5 className="card-title">Profile Details</h5>
                                        <div className="row">
                                            <div className="col-lg-3 col-md-4 label ">Full Name</div>
                                            <div className="col-lg-9 col-md-8">{name}</div>
                                        </div>
                                        <div className="row">
                                            <div className="col-lg-3 col-md-4 label">Location</div>
                                            <div className="col-lg-9 col-md-8">{location}</div>
                                        </div>
                                        <div className="row">
                                            <div className="col-lg-3 col-md-4 label">Email</div>
                                            <div className="col-lg-9 col-md-8">{email}</div>
                                        </div>
                                    </div>
                                    <div className="tab-pane fade profile-edit pt-3" id="profile-edit" >
                                        <ProfileEdit data={{ bio: bio, image: image }} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    );
};


export default NewProfile;