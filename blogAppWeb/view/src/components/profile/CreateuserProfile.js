import React, { useState } from "react";
import axios from "axios";


const styles = {
    preview: {
        marginTop: 50,
        display: "flex",
        flexDirection: "column",
    },
    image: {
        position: 'relative',
        'background-color': '#09f',
        // margin: '20px auto',
        // border: '5px solid red',
        // 'text-align': 'center',
        width: '300px',
        height: '200px',
        border: '1px solid #000',
        // 'border-radius': '200px',
    }
};


const CreateUserProfile = () => {

    const [bio, setBio] = useState();
    const [selectedImage, setSelectedImage] = useState();
    const Token = localStorage.getItem('Token')

    const config = {
        headers: {
            authorization: Token
        }
    };

    const Upload = event => {
        const formData = new FormData();
        formData.append("bio", bio);
        formData.append("file", selectedImage)
        // console.log(formData, cookies);


        axios.post('http://127.0.0.1:8080/profileimg', formData, config)
            .then(response => {
                alert(response.data.message)
                // console.log(response.data.message, 'vvvv');
            }).catch(err => {
                console.log(err);
            })
    }

    const imageChange = (e) => {
        if (e.target.files && e.target.files.length > 0) {
            setSelectedImage(e.target.files[0]);
        }
    };

    const removeSelectedImage = () => {
        setSelectedImage();
    };


    return (
        <>
            <main id="main" class="main">
                <div class="pagetitle">
                    <h1>Create User Profile</h1>
                    <nav>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/">Home</a></li>
                            {/* <li class="breadcrumb-item active"><a href="/">userprofile</a></li> */}
                        </ol>
                    </nav>
                </div>
                <section class="section">
                    <div class="row align-items-top">
                        <div class="col-lg-3">
                            <div class="card">
                                <div class="card-body">
                                    <input type="text" id="bio" placeholder="write your bio" onChange={event => {
                                        const { value } = event.target
                                        setBio(value)
                                    }} />
                                </div>
                                {selectedImage && (
                                    <div style={styles.preview}>
                                        <img
                                            src={URL.createObjectURL(selectedImage)}
                                            style={styles.image}
                                            alt="Thumb"
                                        />
                                    </div>
                                )}
                                {!selectedImage && (
                                    <input
                                        accept="image/*"
                                        type="file"
                                        onChange={imageChange}
                                    />
                                )}
                                
                                <button onClick={removeSelectedImage} style={{ backgroundColor: 'red' }}>
                                    Remove This Image
                                </button>
                                <button onClick={Upload} style={{ backgroundColor: 'lightgreen' }}>
                                    Upload
                                </button>
                            </div>

                        </div>
                    </div>
                </section>
            </main>
        </>
    )
};

export default CreateUserProfile;