import React, { useState } from 'react';
import axios from 'axios';
import './profile.css'
import { Avatar } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import { Button } from 'antd';

const styles = {
    preview: {
        marginTop: 50,
        display: "flex",
        flexDirection: "column",
    },
    image: {
        position: 'relative',
        'background-color': '#09f',
        margin: '20px auto',
        width: '200px',
        height: '200px',
        'border-radius': '200px',
    }
};


const UploadProfile = () => {

    const [bio, setBio] = useState();
    const [selectedImage, setSelectedImage] = useState();
    const Token = localStorage.getItem('Token')

    const config = {
        headers: {
            authorization: Token
        }
    };

    const Upload = event => {
        const formData = new FormData();
        formData.append("bio", bio);
        formData.append("file", selectedImage)
        // console.log(formData, cookies);
        

        axios.post('http://127.0.0.1:8080/profileimg', formData, config)
            .then(response => {
                alert(response.data.message)
                // console.log(response.data.message, 'vvvv');
            }).catch(err => {
                console.log(err);
            })
    }

    const deleteProfile = async(id) => {
        try{
            const removeProfile = await axios.delete(`http://127.0.0.1:8080/?Id${id}`, config);
            console.log(removeProfile.data, 'profile');
        }catch(err){
            console.log(err);

        }
    }

    const imageChange = (e) => {
        if (e.target.files && e.target.files.length > 0) {
            setSelectedImage(e.target.files[0]);
        }
    };

    const removeSelectedImage = () => {
        setSelectedImage();
    };


    return (
        <div className='centered'>
            <div className="card" style={{ width: "18rem" }}>
                <div className='card text-center'>
                    <h4> User Profile</h4>
                    <div id="circle">
                        <Avatar size={100} icon={<UserOutlined />} />
                        <div id="text" >
                            <style dangerouslySetInnerHTML={{ __html: "\nimg {\n  filter: contrast(80%);\n}\n" }} />
                            {selectedImage && (
                                <div style={styles.preview}>
                                    <img
                                        src={URL.createObjectURL(selectedImage)}
                                        style={styles.image}
                                        alt="Thumb"
                                    />
                                </div>
                            )}
                            {!selectedImage && (
                                <input
                                    accept="image/*"
                                    type="file"
                                    onChange={imageChange}
                                />
                            )}
                        </div>
                    </div>
                    <br />
                    <br />
                    <div className="card-body">
                        <div>
                            <input type="text" id="bio" placeholder="write your bio" onChange={event => {
                                const { value } = event.target
                                setBio(value)
                            }} />
                        </div>
                    </div>
                    <Button onClick={removeSelectedImage} style={{ backgroundColor: 'red' }}>
                        Remove This Image
                    </Button>
                    <Button onClick={Upload} style={{ backgroundColor: 'lightgreen' }}>
                        Upload
                    </Button>
                    <button onClick={() => deleteProfile()}>delete</button>
                </div>
            </div>
        </div>
    )
}


export default UploadProfile;