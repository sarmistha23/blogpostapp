import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import axios from 'axios';



const Blog = () => {

    const [blog, setBlog] = useState();
    const [selectedImage, setSelectedImage] = useState();
    const Token = localStorage.getItem('Token');
    const history = useHistory()
    const formData = new FormData();

    const Send = event => {
        formData.append("blog", blog);
        formData.append("file", selectedImage)

        // console.log(selectedImage, 'selectedImage');

        const config = {
            headers: {
                authorization: Token
            }
        }

        axios.post('http://127.0.0.1:8080/blogs', formData, config)
            .then(response => {
                history.push('/')

            }).catch(err => {
                console.log(err);
            });
    };

    const imageChange = (e) => {
        if (e.target.files && e.target.files.length > 0) {
            setSelectedImage(e.target.files[0]);
        };
    };

    const removeSelectedImage = () => {
        setSelectedImage();
    };


    return (
        <main id="main" className="main">
            <div className="pagetitle">
                <h1>create blog</h1>
                <nav>
                    <ol className="breadcrumb">
                        <li className="breadcrumb-item"><a href="/">Home</a></li>
                        <li className="breadcrumb-item">Components</li>
                        <li className="breadcrumb-item active">Cards</li>
                    </ol>
                </nav>
            </div>
            <section className="section">
                <div className="row align-items-top">
                    <div className="col-lg-6">
                        <div className="card mb-3">
                            <div className="row g-0">
                                <div className="col-md-4">
                                    <div style={styles.container}>
                                        {selectedImage && (
                                            <div style={styles.preview}>
                                                <img
                                                    src={URL.createObjectURL(selectedImage)}
                                                    style={styles.image}
                                                    alt="Thumb"
                                                />
                                            </div>
                                        )}
                                        {!selectedImage && (
                                            <input
                                                accept="image/*"
                                                type="file"
                                                onChange={imageChange}
                                            />
                                        )}
                                    </div>
                                </div>
                                <div className="col-md-8">
                                    <div className="card-body">
                                        <h5 className="card-title"><input type="textarea" id="blog" placeholder="Write About Your Post" onChange={event => {
                                            const { value } = event.target
                                            setBlog(value)
                                        }} /></h5>
                                    </div>
                                </div>
                                <button type="button" classNameName="btn btn-primary btn-lg" onClick={Send}>
                                    create
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    )
};
export default Blog;




const styles = {
    container: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        paddingTop: 50,
    },
    preview: {
        marginTop: 50,
        display: "flex",
        flexDirection: "column",
    },
    image: { maxWidth: "100%", maxHeight: 320 },
    delete: {
        cursor: "pointer",
        padding: 15,
        background: "red",
        color: "white",
        border: "none",
    },
};