import React, { useEffect, useState } from 'react'
import { useCookies } from "react-cookie";
import Card from "react-bootstrap/Card";
import axios from 'axios';
import './blog.css';
import { useHistory, Link } from 'react-router-dom';


const GetOneUser = () => {
    const history = useHistory();
    const [userData, setUserData] = useState({ container: [] });

    const Token = localStorage.getItem('Token');


    useEffect(() => {
        // console.log(formData);
        const config = {
            headers: {
                authorization: Token
            }
        };

        const userPost = () => {
            axios.get('http://127.0.0.1:8080/allBlogs', config)
                .then(response => {
                    // alert(response.data.message)
                    // console.log(response.data, 'vvvv');
                    setUserData({ container: response.data.userBlog })
                }).catch(err => {
                    console.log(err);
                })
        }
        userPost();
    }, [setUserData])


    // console.log(userData, 'dadaert');
    const deleteBlog = async (id) => {
        const config = {
            headers: {
                authorization: Token
            }
        }
        try {
            const result = await axios.delete(`http://127.0.0.1:8080/removeBlog?ID=${id}`, config);
            alert(result.data.message)

        } catch (err) {
            console.log(err)
        }
    }




    const renderUserData = (card, index) => {
        const imageUrl = `http://127.0.0.1:8080/${card.image}`
        // console.log(card, 'fffff')

        return (
            
            <div className='centered'>
                <Card style={{ width: "22rem" }} key={card._id}>
                    <Card.Img variant="top" style={{ width: '15rem', height: '15rem' }} src={imageUrl} />
                    <Card.Body>
                        <Card.Text style={{ color: "green" }}>
                            {card.blog}
                        </Card.Text>
                        <Link to={{ pathname: `/editBlog?ID=${card._id}` }}>
                            <button onClick={() => history.push('/editBlog')}>Edit Post</button>
                        </Link>

                        <button onClick={() => deleteBlog(card._id)}>delete</button>
                        
                    </Card.Body>
                </Card>
            </div>
        )
    }


    return (
        <div>
            {userData.container.map(renderUserData)}
        </div>
    )
}



export default GetOneUser;