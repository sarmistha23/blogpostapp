import React, { useState } from 'react';
import axios from 'axios';
import { Button } from 'antd';


const EditBlog = () => {
    const formData = new FormData();
    const [blog, setBlog] = useState();
    const [selectedImage, setSelectedImage] = useState();
    const [outPut, setOutput] = useState({ container: [] });

    const Token = localStorage.getItem('Token')
    const search = window.location.search;
    // console.log(search, 'search');
    const Id = new URLSearchParams(search).get('ID');

    const Send = async () => {
        formData.append("blog", blog);
        formData.append("file", selectedImage)


        // console.log(selectedImage, 'selectedImage')
        // const formDataInJson = JSON.stringify(formData)

        const config = {
            headers: {
                authorization: Token,
            }
        }

        try {
            const result = await axios.put(`http://127.0.0.1:8080/editContent?ID=${Id}`, formData, config);
            setOutput({ container: result.data })
        } catch (err) {
            console.log(err);
        }

    }


    const imageChange = (e) => {
        if (e.target.files && e.target.files.length > 0) {

            setSelectedImage(e.target.files[0]);

        }
    };

    const removeSelectedImage = () => {
        setSelectedImage();
    };


    return (

        <div className='centered'>
            <div className="card" style={{ width: "18rem" }}>
                <div className="card text-center">
                    <h4> Edit Blog </h4>
                    <div id="circle">
                        <div id="text" >
                            <style dangerouslySetInnerHTML={{ __html: "\nimg {\n  filter: contrast(80%);\n}\n" }} />
                            {selectedImage && (
                                <div style={styles.preview}>
                                    <img
                                        src={URL.createObjectURL(selectedImage)}
                                        style={styles.image}
                                        alt="Thumb"
                                    />
                                </div>
                            )}
                            {!selectedImage && (
                                <input
                                    accept="image/*"
                                    type="file"
                                    name='file'
                                    onChange={imageChange}
                                />
                            )}
                        </div>
                    </div>
                    <br />
                    <br />
                    <div className="card-body">
                        <div>
                            <input type="textarea" id="bio" placeholder="type here" onChange={event => {
                                const { value } = event.target
                                setBlog(value)
                            }} />
                        </div>
                    </div>

                    <Button onClick={removeSelectedImage} style={{ backgroundColor: 'red' }}>
                        Remove Image
                    </Button>

                    <button onClick={Send} style={{ backgroundColor: 'lightgreen' }}>
                        Edit Blog
                    </button>
                    <div />
                </div>
                <div />
            </div>
        </div>
    )
}


export default EditBlog;

const styles = {
    preview: {
        marginTop: 50,
        display: "flex",
        flexDirection: "column",
    },
    image: {
        position: 'relative',
        backgroundColor: '#09f',
        margin: '20px auto',
        width: '200px',
        height: '200px',
        borderRadius: '200px',
    }
};