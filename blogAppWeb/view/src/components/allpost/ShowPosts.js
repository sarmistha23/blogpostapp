import React, { useState, useEffect } from "react";
import axios from "axios";
import { useHistory } from "react-router-dom";


const AllPosts = () => {

    const [data, setData] = useState({ dataContainer: [] });
    const [user, setUser] = useState('');
    const history = useHistory()

    useEffect(() => {
        const DataS = async () => {
            await axios.get('http://127.0.0.1:8080/allUsersBlogs')
                .then((result) => {
                    // console.log(result.data.allData, 'bbbbbbb');
                    setData({ dataContainer: result.data.allData })
                    // setImage(`http://127.0.0.1:8080/${resp.data.userProfile.image}`)
                }).catch((err) => {
                    console.log(err);
                    history.push('/error')
                })
        }
        DataS();
    }, [setData]);


    // console.log(data.dataContainer, 'data');


    return (
        <>
            <main id="main" class="main">
                <div class="pagetitle">
                    <h1>Cards</h1>
                    <nav>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/">Home</a></li>
                            <li class="breadcrumb-item">Components</li>
                            <li class="breadcrumb-item active">Cards</li>
                        </ol>
                    </nav>
                </div>
                <section class="section">
                    <div class="row align-items-top">
                        <div class="col-lg-3">
                            {data.dataContainer.map(ele => {
                                const imageUrl = `http://127.0.0.1:8080/${ele.image}`
                                return (
                                    <div class="card" kay={ele._id}>
                                        <img src={imageUrl} class="card-img-top" alt="..." />
                                        {
                                            ele.user.map(name => {
                                                return (
                                                    <div class="card-body" >
                                                        <h5 class="card-title" key={name._id}>{`create by: ${name.name}`}</h5>
                                                        <p class="card-text">{ele.blog}</p>
                                                    </div>
                                                )
                                            })
                                        }
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                </section>
            </main>
        </>
    );
};




export default AllPosts;