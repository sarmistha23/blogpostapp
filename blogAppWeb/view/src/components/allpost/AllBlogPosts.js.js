import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import './blog.css'
import "react-bootstrap/"


const AllPostBlogs = () => {
    const [data, setData] = useState({ dataContainer: [] });
    const [user, setUser] = useState('');
    const history = useHistory()
    
    useEffect(() => {
        const Datas = async () => {
            await axios.get('http://127.0.0.1:8080/allUsersBlogs')
                .then((result) => {
                    // console.log(result.data.allData, 'bbbbbbb');
                    setData({ dataContainer: result.data.allData })
                    // setImage(`http://127.0.0.1:8080/${resp.data.userProfile.image}`)
                }).catch((err) => {
                    console.log(err);
                    history.push('/error')
                })
        }
        Datas();
    }, [setData]);
    // console.log(data, 'data')

    const handleChange = e => {
        // console.log(e.target)
        const { name, value } = e.target
        setUser({
            ...user,
            [name]: value
        })
    }


    const comment = async (id) => {

        try {
            const result = await axios.delete(`http://127.0.0.1:8080/comments?ID=${id}`,);
            console.log(result.data)
        } catch (err) {
            console.log(err);
        }
    }

    const renderCard = (card, index) => {
        // console.log(card.user, 'user');
        const imageUrl = `http://127.0.0.1:8080/${card.image}`;
        return (
            <div className='container' >
                <div className='card-img-top'>
                    <div className="card mb-4">
                        <div className="row g-0">
                            <div className="col-md-4" key={card._id}>
                                <img src={imageUrl} className="img-fluid rounded-start" />
                            </div>
                            <div className="col-md-4">
                                <div className="card-body">
                                    {card.user.map(name => {
                                        return (
                                            <h5 className="card-title" key={name._id}>{`created by: ${name.name}`}</h5>
                                        )
                                    })}
                                </div>
                                <div className='card-body'>
                                    <p className="card-text" key={index}>{card.blog}</p>
                                    <div className='comment'>
                                        <input
                                            type="textarea"
                                            name='comment'
                                            placeholder='write comment....'
                                            value={user.comment}
                                            onChange={handleChange}
                                        />
                                        <button onClick={() => comment(card._id)}>comment</button>
                                    </div>

                                    <button onClick={() => comment(card._id)}>like</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    return (
        <div>
            {data.dataContainer.map(renderCard)}
        </div>
    );
}



export default AllPostBlogs;