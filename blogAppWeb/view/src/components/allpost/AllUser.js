import React, { useState, useEffect, useMemo } from "react";
import axios from "axios";
import * as ReactBootstrap from 'react-bootstrap';

const AllUser = () => {

    const [image, setImage] = useState('');
    const [data, setData] = useState({ postData: [] })

    const Token = localStorage.getItem('Token');

    const config = {
        headers: {
            authorization: Token
        }
    }

    useEffect(() => {
        const DataS = async () => {
            await axios.get("http://127.0.0.1:8080/alluser", config)
                .then((resp) => {
                    setData({ postData: resp.data.findUser })
                    // alert('data get')
                }).catch(err => {
                    console.log(err);
                })
        }
        DataS();

    }, []);

    // console.log(data, 'data');


    return (

        // <ReactBootstrap.Table striped bordered hover>
        //     <thead>
        //         <tr>
        //             <th>name</th>
        //             <th>email</th>
        //             <th>gender</th>
        //             <th>location</th>
        //         </tr>
        //     </thead>
        //     <tbody>
        //         {data.postData && data.postData.map((item) => (
        //             <tr key={item}>
        //                 <td>{item.name}</td>
        //                 <td>{item.email}</td>
        //                 <td>{item.gender}</td>
        //                 <td>{item.location}</td>
        //             </tr>
        //         ))}
        //     </tbody>
        // </ReactBootstrap.Table>


        <>
            <main id="main" className="main">
                <div className="pagetitle">
                    <h1> Users </h1>
                    <nav>
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item"><a href="/">Home</a></li>
                            {/* <li className="breadcrumb-item">Tables</li>
                            <li className="breadcrumb-item active">General</li> */}
                        </ol>
                    </nav>
                </div>
                <section className="section">
                    <div className="row">
                        <div className="col-lg-6">
                            <div className="card">
                                <div className="card-body">
                                    <h5 className="card-title">All Users Data</h5>
                                    <table className="table table-striped">
                                        <thead>
                                            <tr>
                                                <th scope="col">No</th>
                                                <th scope="col">Name</th>
                                                <th scope="col">Gmail</th>
                                                <th scope="col">Gender</th>
                                                <th scope="col">Location</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {data.postData.map((user, index) => {
                                                return (
                                                    <tr key={user._id}>
                                                        <th scope="row">{ index + 1 }</th>
                                                        <td>{user.name}</td>
                                                        <td>{user.email}</td>
                                                        <td>{user.gender}</td>
                                                        <td>{user.location}</td>
                                                    </tr>
                                                )
                                            })}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </main>
        </>
    )
};



export default AllUser;