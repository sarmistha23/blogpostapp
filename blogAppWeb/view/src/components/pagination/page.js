import React, { useState, useEffect } from "react";
import './page.css'
import axios from "axios";
import ReactPaginate from 'react-paginate';


const PaginationPage = () => {
    const [image, setImage] = useState('');
    const [data, setData] = useState({ dataContainer: [] })
    const [offset, setOffset] = useState(0);
    const [perPage] = useState(3);
    const [pageCount, setPageCount] = useState(1)
    const Token = localStorage.getItem('Token');

    const DataS = async (pageNum = 1) => {
        try {
            const result = await axios.get(`http://127.0.0.1:8080/userPage?limit=${3}&page=${pageNum}`)
            const Data = result.data.data;
            // console.log(Data, 'result')
            const totalDocuments = result.data.count;
            const slice = Data.slice(offset, offset + perPage);

            setData({dataContainer: Data});

            setPageCount(Math.ceil(totalDocuments / perPage))
        } catch (err) {
            console.log(err);
        }
    }

    useEffect(() => {
        DataS(pageCount)
    }, [offset])

    const handlePageClick = (e) => {
        DataS(e.selected + 1);
    };


    const renderCard = (card, index) => {
        // console.log(card, 'usersssssss')

        const imageUrl = `http://127.0.0.1:8080/${card.image}`;
        return (
            <div className='container' >
                <div className='card-img-top'>
                    <div className="card mb-4">
                        <div className="row g-0">
                            <div className="col-md-4" key={card._id}>
                                <img src={imageUrl} className="img-fluid rounded-start" />
                            </div>
                            <div className="col-md-4">
                                <div className="card-body" key={card.userId._id}>
                                    {/* {card.map(name => {
                                        return (
                                            <h5 className="card-title">{`created by: ${name.name}`}</h5>
                                        )
                                    })} */}
                                    {card.userId.name}
                                </div>
                                <div className='card-body' key={card._id}>
                                    <p className="card-text">{card.blog}</p>
                                    {/* <div className='comment'> */}
                                    {/* <input
                                        type="textarea"
                                        name='comment'
                                        placeholder='write comment....'
                                        value={user.comment}
                                        onChange={handleChange}
                                    /> */}
                                    {/* <button onClick={() => comment(card._id)}>comment</button> */}
                                    {/* </div> */}

                                    {/* <button onClick={() => comment(card._id)}>like</button> */}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        );
    }

    
    return (
        <div>
            {data.dataContainer.map(renderCard)}
            <ReactPaginate
                    previousLabel={"prev"}
                    nextLabel={"next"}
                    breakLabel={"..."}
                    breakClassName={"break-me"}
                    pageCount={pageCount}
                    marginPagesDisplayed={pageCount}
                    pageRangeDisplayed={3}
                    onPageChange={handlePageClick}
                    containerClassName={"pagination"}
                    subContainerClassName={"pages pagination"}
                    activeClassName={"active"} />
            
        </div>
    );
};


export default PaginationPage;