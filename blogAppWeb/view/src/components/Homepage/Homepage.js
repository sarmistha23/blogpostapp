import React from 'react';
import './homepage.css'
import { useHistory } from 'react-router-dom';


const Homepage = () => {
    const history = useHistory();

    const Logout = async (e) => {

        try {
            let removeToken = await localStorage.removeItem("Token");
            removeToken = await localStorage.removeItem('isAuthenticated');
            history.push('/log');
        } catch (err) {
            console.log(err);
        };
    }


    return (
        <div className='centered'>
            <div className="homepage">
                <h3> Homepage </h3>
                <button onClick={Logout}>logout</button>
            </div>
        </div>
    )
}

export default Homepage;