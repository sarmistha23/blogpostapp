import React, { useState } from "react";
import { Button } from "antd";
import axios from "axios";
import { useHistory } from "react-router-dom";

const OtpVerification = () => {

    const history = useHistory();
    const tokenValue = localStorage.getItem('token')
    // console.log(tokenValue, 'token');
    const [otp, setOtp] = useState({
        otp: ""
    });

    const handleChange = e => {
        const { name, value } = e.target;
        setOtp({
            ...otp,
            [name]: value
        });
    };

    const config = {
        headers: {
            authorization: tokenValue
        }
    };
    
    const oneTPass = async () => {
        try {

            const result = await axios.post('http://127.0.0.1:8080/otpVerify', otp, config);
            // console.log(result, 'result');
            history.push('/password');
            alert('ok')
        } catch (err) {

            console.log(err);
        };
    };

    return (
        <div className='centered'>
            <div className="password">
                <h3> Otp Screen </h3>
                <div class="mb-6" style={{ width: '350px' }}>
                    <label for="exampleFormControlInput1" class="form-label">Enter Otp</label>
                    <input
                        type="text"
                        id="form3Example3c"
                        className="form-control"
                        placeholder="enter otp"
                        name='otp'
                        value={otp.otp}
                        onChange={handleChange}
                    />
                    <br />
                    <br />
                    <Button onClick={oneTPass}>
                        submit
                    </Button>
                </div>
            </div>
        </div>
    )
};


export default OtpVerification;