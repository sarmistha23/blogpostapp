import axios from 'axios';
import React, { useState } from 'react';
import { Button } from "react-bootstrap";
import './password.css';

const EnterPassword = () => {

    const token = localStorage.getItem('token');
    const [data, setData] = useState({
        password: "",
        confirmPassword: ""
    });

    const handleChange = e => {
        const { name, value } = e.target
        setData({
            ...data,
            [name]: value
        })
    };

    // console.log(data, 'data');

    const config = {
        headers: {
            authorization: token
        }
    };

    const send = async () => {

        console.log('hello verification');
        if (!(data.password == data.confirmPassword)) {
            alert('not matched')
        }
        try {

            const result = await axios.put('http://127.0.0.1:8080/setPassword', data, config);
            console.log(result.data, 'result');
            // const tokenValue = result.data.generateToken;
            localStorage.removeItem('token');
            alert('ok');
        } catch (err) {
            console.log(err);
        };
    };

    // console.log('data', data);

    return (

        // <>
            <form>
                <div class="row mb-3">
                    <label for="newPassword" class="col-md-4 col-lg-3 col-form-label">password</label>
                    <div class="col-md-8 col-lg-9">
                        <input name="password" 
                        type="password" 
                        class="form-control" 
                        id="password" 
                        value={data.password}
                        onChange={handleChange}
                        />
                    </div>
                </div>

                <div class="row mb-3">
                    <label for="renewPassword" class="col-md-4 col-lg-3 col-form-label">Re Enter Password</label>
                    <div class="col-md-8 col-lg-9">
                        <input name="confirmPassword" 
                        type="password" 
                        class="form-control" 
                        id="confirmPassword" 
                        value={data.confirmPassword}
                        onChange={handleChange} />
                    </div>
                </div>

                <div class="text-center">
                    <button type="submit" class="btn btn-primary" onClick={send}>Change Password</button>
                </div>
            </form>
        // </>



        // <div className='centered'>
        //     <div className="password">
        //         <h3> Enter New Password </h3>
        //         <div class="mb-6" style={{ width: '350px' }}>
        //             <label for="exampleFormControlInput1" class="form-label"> password </label>
        //             <input 
        //                 type="password"
        //                 id="form3Example3c"
        //                 className="form-control"
        //                 placeholder="password"
        //                 name='password'
        //                 value={data.password}
        //                 onChange={handleChange}
        //             />
        //         </div>
        //         <div class="mb-6" style={{ width: '350px' }}>
        //             <label for="exampleFormControlInput1" class="form-label">Re Enter Password</label>
        //             <input
        //                 type="password"
        //                 id="form3Example3c"
        //                 className="form-control"
        //                 placeholder="Re Enter Password"
        //                 name='confirmPassword'
        //                 value={data.confirmPassword}
        //                 onChange={handleChange}
        //             />
        //             <br />
        //             <br />
        //             <Button onClick={send}>
        //                 submit
        //             </Button>
        //         </div>
        //     </div>
        // </div>
    );
};



export default EnterPassword;