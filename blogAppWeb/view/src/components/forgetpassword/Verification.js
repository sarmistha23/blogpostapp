import axios from 'axios';
import React, { useState } from 'react';
import { Button } from "react-bootstrap";
import { useHistory } from 'react-router-dom';
import './password.css';


const VerifyEmail = () => {
    const history = useHistory();
    const [data, setData] = useState({
        email: ""
    });

    const handleChange = e => {
        const { name, value } = e.target
        setData({
            ...data,
            [name]: value
        })
    };

    // console.log(data, 'data');

    const send = async() => {
        console.log('hello verification');
        try{
            const result = await axios.post('http://127.0.0.1:8080/verification', data);
            console.log(result.data, 'result');
            const tokenValue = result.data.generateToken;
            localStorage.setItem('token', tokenValue);
            history.push('/screen');
            // alert('ok');
            
        } catch(err){
            console.log(err);
        };
    };

    // console.log('email', email);

    return (
        <div className='centered'>
            <div className="password">
                <h3>Email For Otp </h3>
                <div class="mb-6" style={{ width: '350px' }}>
                    <label for="exampleFormControlInput1" class="form-label">Email address</label>
                    <input
                        type="email"
                        id="form3Example3c"
                        className="form-control"
                        placeholder="name@example.com"
                        name='email'
                        value={data.email}
                        onChange={handleChange}
                    />
                    <br />
                    <br />
                    <Button onClick={send}>
                        submit
                    </Button>
                </div>
            </div>
        </div>
    );
};



export default VerifyEmail;