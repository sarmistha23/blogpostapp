import axios from "axios";
import React, { useEffect, useState } from "react";



const SearchBar = () => {

    const [Search, setSearch] = useState({
        searchbar: ""
    });

    const handleChange = e => {
        const { name, value } = e.target
        setSearch({
            ...Search,
            [name]: value
        })
    }

    const searchUser = async (e) => {
        e.preventDefault();
        // console.log('hello search user name');
        try {

            const searchResult = await axios.get(`http://127.0.0.1:8080/searchBlog?name=${Search.searchbar}`);
            console.log(searchResult, 'hello search bar');

        } catch (error) {
            console.log(error);
        }
    }


    return (
        <div className="container">
            <form className="form-inline my-2 my-lg-0">
                <input className="form-control mr-sm-2"
                    type="search"
                    placeholder="Search"
                    aria-label="Search"
                    name='searchbar'
                    value={Search.searchbar}
                    onChange={handleChange}
                />
                <button className="btn btn-outline-success my-2 my-sm-0"
                    type="submit"
                    onClick={searchUser}
                >Search</button>
            </form>
        </div>
    )
}



export default SearchBar;