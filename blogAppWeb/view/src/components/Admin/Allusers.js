import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";


const AdminCheckAllUsersData = () => {

    const history = useHistory();

    const [image, setImage] = useState('');
    const [data, setData] = useState({ postData: [] })

    const Token = localStorage.getItem('Token');

    const config = {
        headers: {
            authorization: Token
        }
    }

    useEffect(() => {
        const DataS = async () => {
            await axios.get("http://127.0.0.1:8080/admin/allUsers", config)
                .then((resp) => {
                    console.log(resp.data, 'allUsers');
                    if(resp.data.allUsers.length === 0){
                        history.push('/error')
                    }
                    setData({ postData: resp.data.allUsers })
                    // alert('data get')
                }).catch(err => {
                    console.log(err,);
                });
        }
        DataS();

    }, []);

    // console.log(data, 'result');

    const deleteUser = async (ID, proId) => {
        try {
            const user = await axios.delete(`http://127.0.0.1:8080/admin/userDelete?proId=${proId}&ID=${ID}`, config)
            console.log(user, 'user');
        } catch (err) {
            console.log(err, 'error');
        };

    };

    return (
        <main id="main" className="main">
            <div className="pagetitle">
                <h1>Users profile</h1>
                <nav>
                    <ol className="breadcrumb">
                        <li className="breadcrumb-item"><a href="/">Home</a></li>
                        {/* <li className="breadcrumb-item">Components</li>
                        <li className="breadcrumb-item active">Cards</li> */}
                    </ol>
                </nav>
            </div>
            <section className="section">
                <div className="row align-items-top">
                    <div className="col-lg-3">
                        {data.postData.map(user => {
                            { console.log(user._id, 'inside div') }
                            if (user.userId !== null) {
                                return (
                                    <div className="card" key={user._id}>
                                        <div className="card-body">
                                            <h5 className="card-title">{`name: ${user.userId.name}`}</h5>
                                            <p className="card-text">{`email: ${user.userId.email}`}</p>
                                            <p className="card-text">{`Gender: ${user.userId.gender}`}</p>
                                            <p className="card-text">{`Location: ${user.userId.location}`}</p>
                                            <img src={`http://127.0.0.1:8080/${user.image}`} className="card-img-bottom" alt="..." />
                                            <button type="button" class="btn btn-danger rounded-pill" onClick={() => deleteUser(user.userId._id, user._id)}>Delete</button>
                                        </div>
                                    </div>
                                )
                            } else{
                                return(
                                    <div className="card" key={user._id}>
                                        <div className="card-body">
                                            <h5 className="card-title">user data not exists</h5>
                                            <img src={`http://127.0.0.1:8080/${user.image}`} className="card-img-bottom" alt="..." />
                                            <button type="button" class="btn btn-danger rounded-pill" onClick={() => deleteUser(user.userId._id, user._id)}>Delete</button>
                                        </div>
                                    </div>
                                )
                            }
                        })}
                    </div>
                </div>
            </section>
        </main>
    );
};


export default AdminCheckAllUsersData;