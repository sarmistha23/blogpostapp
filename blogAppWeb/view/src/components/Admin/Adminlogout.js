import React,{useState} from "react";
import { useHistory } from "react-router-dom";

const AdminLogoUtPage = () => {

    const history = useHistory();

    const Logout = async (e) => {

        try {
            let removeToken = await localStorage.removeItem("Token");
            removeToken = await localStorage.removeItem('isAuthenticated');
            history.push('/admin/log');
        } catch (err) {
            console.log(err);
        };
    }


    // return (
    //     <div className='centered'>
    //         <div className="homepage">
    //             <h3> Homepage </h3>
    //             <button onClick={Logout}>logout</button>
    //         </div>
    //     </div>
    // );




    return (
        <main id="main" class="main">
            <div class="pagetitle">
                <h1>LogOut page</h1>
                <nav>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin">Admin Home page</a></li>
                        {/* <li class="breadcrumb-item">Components</li>
                        <li class="breadcrumb-item active">Modal</li> */}
                    </ol>
                </nav>
            </div>
            <section class="section">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Admin Log Out</h5>
                                <p>If you out then your account would not be shown to anyone. You have to login again</p>
                                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#basicModal">
                                    logout
                                </button>
                                <div class="modal fade" id="basicModal" tabindex="-1">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">R u sure want ot logout?</h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary" onClick={Logout}>yes</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    );
};



export default AdminLogoUtPage;