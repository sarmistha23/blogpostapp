import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";


const AdminLogin = () => {

    const history = useHistory();
    const [errorMessage, setErrorMessage] = useState('');

    const [user, setUser] = useState({
        email: "",
        password: ""
    });

    const handleChange = e => {
        
        // console.log('hello login');

        const { name, value } = e.target
        setUser({
            ...user,
            [name]: value
        })
    };

    console.log(user, 'user');

    const logIn = async (event) => {

        event.preventDefault();
        // console.log('hello login user');

        try {

            const result = await axios.post('http://127.0.0.1:8080/admin/login', user);

            // console.log('hello login user');
            // console.log(result.data, 'result');

            if (result.data.message == "logged In successful") {
                localStorage.setItem('Token', result.data.token);
                localStorage.setItem('isAuthenticated', true);
                alert('ok')
                history.push('/newprofile');
            };
            // if (result.data.message == 'username or email something is wrong') {
            //     history.push('/sign');
            // }
            // if (result.data.message == false) {
            //     alert('wrong password')
            // }

        } catch (error) {
            console.log(error.response.data.message);
            setErrorMessage(error.response.data.message)
        }
    }

    return (

        <div className="container">
            <section className="section register min-vh-100 d-flex flex-column align-items-center justify-content-center py-4">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-lg-4 col-md-6 d-flex flex-column align-items-center justify-content-center">
                            <div className="d-flex justify-content-center py-4">
                                <a href="index.html" className="logo d-flex align-items-center w-auto">
                                    <img src="assets/img/logo.png" alt="" />
                                    <span className="d-none d-lg-block">Admin Login Page</span>
                                </a>
                            </div>
                            {/* <!-- End Logo --> */}
                            <div className="card mb-3">
                                <div className="card-body">
                                    <div className="pt-4 pb-2">
                                        <h5 className="card-title text-center pb-0 fs-4">Create an Account</h5>
                                        <p className="text-center small">Enter your personal details to create account</p>
                                    </div>
                                    <form className="row g-3 needs-validation" validate>

                                        <div className="col-12">
                                            <label for="yourUsername" className="form-label">Your Email</label>
                                            <div className="input-group has-validation">
                                                <span className="input-group-text" id="inputGroupPrepend">@</span>
                                                <input type="email" name="email" className="form-control" id="UserEmail" required
                                                    value={user.email}
                                                    onChange={handleChange}
                                                />
                                                <div className="invalid-feedback">Please enter a valid Email address!</div>
                                            </div>
                                        </div>

                                        <div className="col-12">
                                            <label for="yourEmail" className="form-label">Choose Password</label>
                                            <input type="password" name="password" className="form-control" id="password" required
                                                value={user.password}
                                                onChange={handleChange}
                                            />
                                            <div className="invalid-feedback">please enter valid password</div>
                                        </div>

                                        <div className="col-12">
                                            <div className="form-check">
                                                <input className="form-check-input" name="terms" type="checkbox" value="" id="acceptTerms" required />
                                                <label className="form-check-label" for="acceptTerms">I agree and accept the <a href="#">terms and conditions</a></label>
                                                <div className="invalid-feedback">You must agree before submitting.</div>
                                            </div>
                                        </div>

                                        <div className="col-12">
                                            <button className="btn btn-primary w-100" type="submit" onClick={logIn}>login to account</button>
                                        </div>
                                        <div className="col-12">
                                            <p className="small mb-0">Already have an account? <a href="pages-login.html">Log in</a></p>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    );
};



export default AdminLogin;