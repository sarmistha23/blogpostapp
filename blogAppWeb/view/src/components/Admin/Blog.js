import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";


const BlogControl = () => {

    const [data, setData] = useState({ dataContainer: [] });
    const [user, setUser] = useState('');
    const history = useHistory()

    const Token = localStorage.getItem('Token');

    const config = {
        headers: {
            authorization: Token
        }
    };

    useEffect(() => {
        const Datas = async () => {
            await axios.get('http://127.0.0.1:8080/allUsersBlogs')
                .then((result) => {
                    // console.log(result.data.allData, 'bbbbbbb');
                    setData({ dataContainer: result.data.allData })
                    // setImage(`http://127.0.0.1:8080/${resp.data.userProfile.image}`)
                }).catch((err) => {
                    console.log(err);
                    history.push('/error')
                })
        }
        Datas();
    }, [setData]);
    // console.log(data, 'data')

    const deleteBlog = async(ID) => {
        try{

            const deletedData = await axios.delete(`http://127.0.0.1:8080/admin/blogDelete?ID=${ID}`, config);

            console.log(deletedData, 'data');
            alert('ok')
        } catch(err){
            console.log(err);
        }
    }


    return (

        <main id="main" class="main">
            <div class="pagetitle">
                <h1>Cards</h1>
                <nav>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        {/* <li class="breadcrumb-item">Components</li>
                        <li class="breadcrumb-item active">Cards</li> */}
                    </ol>
                </nav>
            </div>
            <section class="section">
                <div class="row align-items-top">
                    <div class="col-lg-6">
                        {data.dataContainer.map(user => {
                            return (
                                <div class="card mb-3" key={user._id}>
                                    {/* {console.log(user, 'userdata')} */}
                                    <div class="row g-0">
                                        <div class="col-md-4">
                                            <img src={`http://127.0.0.1:8080/${user.image}`} class="img-fluid rounded-start" alt="..." />
                                        </div>
                                        <div class="col-md-8">
                                            <div class="card-body">
                                                {/* <h5 class="card-title">{`created by: ${user.user.userId}`}</h5> */}
                                                <p class="card-text">{user.blog}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-danger rounded-pill" onClick={() => deleteBlog(user._id)}>Delete</button>
                                </div>
                            )
                        })}
                    </div>
                </div>
            </section>
        </main>
    );
};


export default BlogControl;