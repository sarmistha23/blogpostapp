import React from "react"
import { NavLink } from "react-router-dom";
import SearchBar from "../searchbar/Searchbar";



const Nav = () => {

    const state = localStorage.getItem('isAuthenticated');
    // console.log(state, 'state');

    const RenderMenu = () => {
        if (state) {
            return (
                <>
                    <li className="nav-item">
                        <NavLink exact activeClassName="active-page" className="dropdown-item" to='/'>blogs</NavLink>
                    </li>


                    <li className="nav-item">
                        <NavLink exact activeClassName="active-page" className="dropdown-item" to='/home'>logout</NavLink>
                    </li>
                </>
            )
        } else {
            return (
                <>
                    <li className="nav-item">
                        <NavLink exact activeClassName="active-page" className="dropdown-item" to='/sign'>Signup</NavLink>
                    </li>

                    <li className="nav-item">
                        <NavLink exact activeClassName="active-page" className="dropdown-item" to='/login'>LogIn</NavLink>
                    </li>

                    <li className="nav-item">
                        <NavLink exact activeClassName="active-page" className="dropdown-item" to='/'>blogs</NavLink>
                    </li>
                </>
            );
        };
    };

    return (
        <>
            <nav className="navbar navbar-expand-lg navbar-light" style={{ backgroundColor: "lightgrey" }}>

                <li className="nav-item dropdown">

                    <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        User
                    </a>

                    <div className="dropdown-menu" aria-labelledby="navbarDropdown">

                        <NavLink exact activeClassName="active-page" className="dropdown-item" to='/profileimg'>profile</NavLink>

                        <NavLink exact activeClassName="active-page" className="dropdown-item" to='/userprofile'>getProfile</NavLink>

                        <NavLink exact activeClassName="active-page" className="dropdown-item" to='/updateProfile'>updateProfile</NavLink>

                        <div class="dropdown-divider"></div>
                    </div>
                </li>

                {/* <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button> */}

                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto">
                        <RenderMenu />
                    </ul>
                    <div className="container">
                        {/* <form className="form-inline my-2 my-lg-0">
                            <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />
                            <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                        </form> */}
                        {/* <SearchBar/> */}
                    </div>
                </div>
            </nav>
        </>
    );
};



export default Nav;