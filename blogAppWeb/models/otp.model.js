const mongoose = require('mongoose');

const schema = mongoose.Schema({
    oTimePass: {
        type: String,
        required: true
    }, userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'users'
    }
});

const otpModel = new mongoose.model('otpes', schema);

module.exports = otpModel;