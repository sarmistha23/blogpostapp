const mongoose = require('mongoose');

const adminSchema = new mongoose.Schema({
    name: {
        type: String
    }, email: {
        type: String,
        index: true,
        unique: true,
        dropDups: true,
        sparse: true
    }, password: {
        type: String
    }, gender: {
        type: String,
        enum : ['male','female', 'other'], 
    }, location: {
        type: String,
        default: ' '
    }, role: {
        type: String,
        enum: ['admin']
    }
});



const adminModel = new mongoose.model('admins', adminSchema);

module.exports = adminModel;