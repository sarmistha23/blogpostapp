const adminModel = require('../../models/admin');
const blogModel = require('../../models/blog.model');
const userModel = require('../../models/user.model');
const profileModel = require('../../models/profilepic');
// const Joi = require('joi');


const adminDeleteUsersBlog = async (decoded, req, res, next) => {
    
    console.log(req.query.ID, 'blogId');

    // console.log(decoded, 'decoded');

    if (decoded.role != 'admin') {
        return res.status(400).send({
            status: 400,
            message: "only admin can delete any users blog"
        });
    };

    try {

        const deleteUserBlog = await blogModel.findByIdAndDelete({ _id: req.query.ID});

        console.log(deleteUserBlog, 'deleteUserBlog');

        return res.status(200).send({
            status: 200,
            message: 'delete successful'
        });

    } catch (err) {
        console.log(err, 'inside catch blog');
        return res.status(500).send({
            status: 500,
            err
        });
    };
};




const adminDeleteUserAcc = async(decoded, req, res, next) => {

    console.log(req.query.proId, 'userAccId');

    if(decoded.role != 'admin'){
        return res.status(400).send({
            status: 400,
            message: "don't have access to delete user"
        });
    };

    try{

        const deleteUser = await userModel.findByIdAndDelete({_id: req.query.ID});
        const deleteProfile = await profileModel.findByIdAndDelete({_id: req.query.proId})
        // console.log(deleteUser, 'users');
        console.log(deleteProfile, 'deleteProfile');

        return res.status(200).send({
            status: 200,
            message: 'user Acc Delete successful'
        });

    } catch(err){
        console.log(err);
        return res.status(400).send({
            status: 400,
            err
        });
    };
};


module.exports = {
    adminDeleteUsersBlog,
    adminDeleteUserAcc
};