const adminModel = require('../../models/admin');
const profileModel = require('../../models/profilepic');
const salt = parseInt(process.env.SALT);
const Joi = require('joi');
const bcrypt = require('bcrypt');
const Token = require('../../services/createtoken');


const adminSignup = async (req, res) => {

    // console.log("req.body", req.body);

    const adminSchema = Joi.object({
        name: Joi.string().required(),
        email: Joi.string().email().required(),
        password: Joi.string().required(),
        location: Joi.string(),
        gender: Joi.string().valid('male', 'female'),
        role: Joi.string().default('admin').allow()
    });

    // console.log("adminSchema", adminSchema);

    let adminData = adminSchema.validate(req.body);
    // console.log(adminData, 'adminData');

    if (adminData.error) {
        console.log(adminData.error, 'adminData.error');

        return res.status(400).send({
            message: adminData.error.details[0].message,
            status: 400
        });

    } else {
        adminData = adminData.value;
    };


    try {

        // console.log('inside try block');

        const hashPassword = await bcrypt.hash(adminData.password, salt);

        const adminSignupData = {
            name: adminData.name,
            email: adminData.email,
            password: hashPassword,
            location: adminData.password,
            gender: adminData.gender,
            role: adminData.role
        };

        const newAdmin = await adminModel.create(adminSignupData);

        return res.status(201).send({
            newAdmin,
            status: 201
        });

    } catch (err) {

        // console.log('inside catch');

        console.log(err);

        return res.status(500).send({
            err,
            status: 500
        });
    };
};


const adminLogin = async (req, res) => {

    // console.log(req.body, 'login body');

    const logValidation = Joi.object({
        email: Joi.string().email().required(),
        password: Joi.string().required()

    });

    let validation = logValidation.validate(req.body);
    if (validation.error) {
        return res.status(201).send({
            status: 400,
            message: validation.error.details[0].message
        });
    } else {
        validation = validation.value;
    };


    try {

        const existsAdmin = await adminModel.findOne({ email: validation.email });

        // console.log(existsAdmin, 'existsAdmin');

        const isMatchedPassword = await bcrypt.compare(validation.password, existsAdmin.password);

        // console.log(isMatchedPassword, 'IsMatchedPassword');
        let token;
        if (isMatchedPassword) {
            const payload = {
                id: existsAdmin._id,
                email: existsAdmin.email,
                role: existsAdmin.role
            };
            token = await Token.createToken(payload);
            console.log(token, 'token for previous work');
        };

        return res.status(200).send({
            status: 200,
            message: 'logged In successful',
            token
        });


    } catch (err) {
        console.log(err);
        return res.status(500).send({
            status: 500,
            err
        });
    };
};


const AllUsersProfile = async (decoded, req, res, next) => {

    // console.log(decoded, 'decoded');


    try {

        if (decoded.role == 'admin') {
            const allUsers = await profileModel.find({}).populate('userId');
            
            console.log(allUsers, 'allUsers');
            

            return res.status(200).send({
                status: 200,
                allUsers
            });
        };

    } catch (err) {
        console.log(err, 'error');
        return res.status(500).send({
            status: 500,
            err
        });
    };
};



module.exports = {
    adminSignup,
    adminLogin,
    AllUsersProfile
};