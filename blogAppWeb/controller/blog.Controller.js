const Joi = require('joi');
const { Console } = require('winston/lib/winston/transports');
const blogModel = require('../models/blog.model');
const userModel = require('../models/user.model');
const checkWords = require('../services/checkwords');
const companyWallet = require('../models/company');
const userWallet = require('../models/userWallet');


const createBlog = async (decoded, req, res, next) => {

    // console.log(decoded, 'decode');
    // console.log(req.file);

    const validation = Joi.object({
        blog: Joi.string(),
        image: Joi.string()
    });

    let validationOfContent = validation.validate(req.body);

    if (validationOfContent.error) {
        return res.status(400).send({
            status: 400,
            error: validationOfContent.error
        });

    } else {
        validationOfContent = validationOfContent.value;
    };

    try {
        const userName = await userModel.findOne({ email: decoded.email });
        const lenthOfContent = await checkWords.checkWords(validationOfContent.blogContent);
        if (lenthOfContent > 200) {
            res.send('content should have less than 200 words')
        };
        const blogData = {
            blog: validationOfContent.blog,
            image: req.file.path,
            userId: userName._id
        };

        const createBlog = await blogModel.create(blogData);

        const userCoins = await userWallet.findOne({ userId: decoded.userId });
        const deductCoins = {
            coins: userCoins.coins - 10
        };

        const userID = { userId: decoded.userId };

        const updateUserWallet = await userWallet.findOneAndUpdate(userID, deductCoins, { new: true });

        const walletOfCom = await companyWallet.findById({ _id: '61dc2c3c739c04ece8fe423f' });

        const addOnComWallet = {
            ComWallet: walletOfCom.ComWallet + 10
        };

        const updateCompanyWallet = await companyWallet
            .findByIdAndUpdate({ _id: '61dc2c3c739c04ece8fe423f' },
                addOnComWallet, { new: true });
        return res.status(201).send({
            status: 201,
            message: 'blog created'
        });
    } catch (err) {
        console.log(err);
        return res.status(400).send({
            status: 400,
            err
        });
    };
};



const editBlog = async (decoded, req, res, next) => {

    const validation = Joi.object({
        blog: Joi.string(),
        image: Joi.string()
    });

    let contentValidation = validation.validate(req.body);

    if (contentValidation.error) {
        return res.status(400)
            .send(contentValidation.error.details[0].message);
    } else {
        contentValidation = contentValidation.value;
    };

    const payload = {
        blog: contentValidation.blog,
        image: req.file.path
    };

    try {

        const updateData = await blogModel
            .findByIdAndUpdate({ _id: req.query.ID },
                payload, { new: true }
            );

        return res.status(202).send({
            status: 202,
            message: 'success',
            updateData
        });
    } catch (err) {
        console.log(err);
        return res.status(400).send(err)
    };
};



const getBlog = async (decoded, req, res, next) => {

    try {

        const userBlog = await blogModel.find({ userId: decoded.userId }, { userId: 0, __v: 0 });
        return res.status(200)
            .send({
                status: 200,
                userBlog
            });

    } catch (err) {
        return res.status(400).send({
            status: 400,
            message: err
        });
    };
};




const removeBlog = async (decoded, req, res, next) => {
    try {

        const deleteBlog = await blogModel.deleteOne({ _id: req.query.ID });
        console.log(deleteBlog, 'deleteBlog')
        return res.status(202).send({
            status: 202,
            message: 'blog successful deleted'
        });

    } catch (err) {
        console.log(err, 'error');
        return res.status(417).send({
            status: 417,
            message: err
        });
    };
};



module.exports = {
    createBlog,
    editBlog,
    removeBlog,
    getBlog
};