module.exports.userController = require('./user.controller');
module.exports.blogController = require('./blog.Controller');
module.exports.commentController = require('./comment.controller');
module.exports.forgetPass = require('./forgetPass');
module.exports.profilepic = require('./profileimg.controller');
module.exports.otpVerification = require('./page');
module.exports.searchController = require('./search.controller');
// module.exports.adminController = require('./admin.controller');
module.exports.adminController = require('./admin/admin.controller');
module.exports.adminControlUserBlog = require('./admin/blog');