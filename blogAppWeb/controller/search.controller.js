const userModel = require('../models/user.model');
const blogModel = require('../models/blog.model');

const searchBarUser = async (req, res) => {
    // console.log(req.query);

    try {
        const result = await userModel.find({ name: req.query.name });
        // console.log(result, 'search result');
        return res.status(200).send({
            status: 200,
            result
        });

    } catch (err) {
        console.log(err);
        return res.status(500).send({
            status: 500,
            err
        });
    };
};




const searchPosts = async (req, res) => {

    // console.log(req.query.name, 'queryertyj');

    const searchName = req.query.name;

    try {

        const result = await blogModel
            .find({})
            .populate('userId')


        const matchedUser = await result.filter((name) => {
            return name.userId.name == searchName;
        });

        return res.status(200).send({
            status: 200,
            matchedUser
        });

    } catch (err) {
        console.log(err);
        return res.status(200).send({
            status: 500,
            err
        });
    };
};




module.exports = {
    searchBarUser,
    searchPosts
};