const blogModel = require('../models/blog.model');
const otpModel = require('../models/otp.model');
const userModel = require('../models/user.model');
const Joi = require('joi');
const bcrypt = require('bcrypt');
const salt = parseInt(process.env.SALT)


const pagination = async (req, res) => {
    const limit = req.query.limit ? parseInt(req.query.limit) : 3;
    // console.log(limit, 'limit');

    const pageNumber = req.query.page ? parseInt(req.query.page) : 1;

    // console.log(pageNumber, 'endPageNumber');
    const countDocuments = await blogModel.count();
    // console.log(countDocuments, 'documents');

    blogModel.find({})
        .populate('userId')
        //skip takes argument to skip number of entries 
        .sort({})
        .skip((pageNumber - 1) * limit)
        //limit is number of Records we want to display
        .limit(limit)
        .then(data => {
            console.log(data, 'inside then');
            return res.status(200).send({
                count: countDocuments,
                data
            });
        })
        .catch(err => {
            console.log(err, "inside catch");
            return res.status(400).send({
                "err": err
            });
        });
};



const otpVerification = async (decoded, req, res, next) => {
    console.log(decoded, 'decoded');
    console.log((req.body.otp), 'post');

    try {

        const findOtp = await otpModel.findOne({ userId: decoded.userId });
        console.log((findOtp.oTimePass), 'otp');
        if (findOtp.oTimePass == req.body.otp) {
            // console.log('hello otp')
            return res.status(200).send({
                status: 200,
                message: 'successful'
            });
        } else {
            return res.status(400).send({
                message: 'incorrect otp'
            });
        };

    } catch (err) {
        console.log(err);
        return res.status(400).send({
            error: err
        });
    };
};



const enterPassword = async (decoded, req, res, next) => {
    console.log(decoded, 'decoded');
    console.log("hello password")
    console.log(req.body, 'body data')
    if (!(req.body.password == req.body.confirmPassword)) {
        return res.status(400).send({
            status: 400,
            message: 'both password are not same'
        });
    }
    const Validate = Joi.object({
        password: Joi.string().alphanum().required(),
        confirmPassword: Joi.string().alphanum().required()
    });

    let passwordValidation = Validate.validate(req.body);
    if (passwordValidation.error) {
        return res.status(400).send({
            status: 400,
            message: passwordValidation.error.details[0].message
        });
    } else {
        passwordValidation = passwordValidation.value;
    };
    console.log(passwordValidation, 'password');

    try {
        const hashPassword = await bcrypt.hash(passwordValidation.password, salt);

        const passwordData = {
            password: hashPassword
        };

        const editPassword = await userModel.findByIdAndUpdate({ _id: decoded.userId }, passwordData, { new: true });
        console.log(editPassword, 'password');

        return res.status(200).send({
            status: 200,
            message: 'update done'
        });
    } catch (err) {
        console.log(err, 'error');
        return res.status(400).send({
            status: 400,
            err
        });
    };
};


// const reSendOtp = async(req, res) => {
//     console.log(req.body, 'hello resend');
//     try{

//     } catch(err){
//         console.log(err);
//         return res.status(500).send({
//             status: 500,
//             err
//         });
//     };
// };


module.exports = {
    pagination,
    otpVerification,
    enterPassword
};