const userModel = require('../models/user.model');
const bcrypt = require('bcrypt');
const salt = parseInt(process.env.SALT)
const commentModel = require('../models/comment.model');
const blogModel = require('../models/blog.model');
const profileModel = require('../models/profilepic');
const Joi = require('joi');
const OTP = require('../services/generatecode');
const nodeMailer = require('../services/mailverification');
const otpModel = require('../models/otp.model');
const Jwt = require('jsonwebtoken');


const forgetPass = async (req, res) => {

    const dataOfEmail = Joi.object({
        email: Joi.string().email().required()
    });

    const userData = {
        email: req.body.email
    };

    try {
        const hashPassword = await bcrypt.hash(userData.password, salt);
        const updatePass = {
            password: hashPassword
        };
        const setPassword = await userModel.findOneAndUpdate({ email: userData.email }, updatePass, { new: true });
        return res.status(200).send({
            status: 200,
            message: 'successful'
        });

    } catch (err) {
        console.log(err);
        return res.status(500).send({
            message: 'error'
        })
    }
};



const AllBlogPosts = async (req, res) => {
    // const allData = await commentModel.find({ _id: req.query.ID })
    //     .populate('userId')
    //     .populate('blogId')

    try {
        const allData = await blogModel.aggregate([{
            $lookup: {
                from: 'users',
                localField: 'userId',
                foreignField: '_id',
                as: 'user'
            }
        },
        {
            $lookup: {
                from: 'comments',
                localField: '_id',
                foreignField: 'blogId',
                as: 'comment'
            }
        }
        ])
        // console.log(allData, 'iiiii');

        return res.status(202)
            .send({
                status: 202,
                allData
            })
    } catch (err) {
        // console.log(err);
        return res.send({
            status: 400,
            message: err
        })
    }
}



const getUserProfile = async (decoded, req, res, next) => {
    try {
        const userProfile = await profileModel.findOne({ userId: decoded.userId })
            .populate('userId');

        // console.log(userProfile, 'profile');

        // const Datas = {
        //     image: userProfile.image,
        //     name: userProfile.userId.name,
        //     bio: userProfile.bio
        // }
        return res.status(202).send({
            status: 202,
            userProfile
        });
    } catch (err) {
        return res.status(400).send({
            status: 400,
            err
        });
    };
};



const verificationEmail = async (req, res) => {
    console.log(req.body, 'body data');

    const data = Joi.object({
        email: Joi.string().email().required()
    });

    let validateEmail = data.validate(req.body);

    if (validateEmail.error) {
        return res.status(300).send({
            message: 'email not valid'
        });
    } else {
        validateEmail = validateEmail.value;
    };

    try {
        const searchUser = await userModel.findOne({ email: validateEmail.email });
        const generateOtp = await OTP.generateRandomCode();
        const otpSendToUser = await nodeMailer.mailSender(validateEmail.email, generateOtp, '');

        const data = {
            oTimePass: generateOtp,
            userId: searchUser._id
        };

        const createToken = (data) => {
            return Jwt.sign(data, process.env.SERECTKEY)
        };

        const generateToken = await createToken(data);
        const storeOtp = await otpModel.create(data);

        return res
            .status(200)
            .send({
                status: 200,
                message: 'successful',
                generateToken
            });

    } catch (err) {
        console.log(err);
    }
};



// {
//     expiresIn: 5 * 3600
// }


module.exports = {
    forgetPass,
    AllBlogPosts,
    getUserProfile,
    verificationEmail
};