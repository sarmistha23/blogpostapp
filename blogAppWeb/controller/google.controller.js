const userModel = require('../models/user.model');
const TokenFun = require('../services/createtoken');
// console.log(createToken, 'token');


exports.googleLogin = async (req, res) => {

    try {
        const user_info = req.user._json;

        // res.redirect('http://localhost:3000/');          //google-auth-login

        const existingUser = await userModel.findOne({ email: user_info.email });
        // console.log(existingUser, 'users');

        if (existingUser != null) {
            const payload = {
                userId: existingUser._id,
                email: existingUser.email
            };

            const Token = await TokenFun.createToken(payload);
            // console.log(Token, 'Token');
            res.cookie('token', Token, {
                httpOnly: true,
                maxAge: 1000 * 60 * 60 * 24 * 356
            });

            console.log('login successful');

            res.redirect('http://localhost:3000/');

        } else {
            res.redirect('http://localhost:3000/sign');
        };
    } catch (err) {
        console.log(err);
        res.send(err);
    }
};